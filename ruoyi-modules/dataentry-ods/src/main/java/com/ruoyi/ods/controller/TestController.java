package com.ruoyi.ods.controller;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * 劳派员工信息Controller
 *
 * @author lhj
 * @date 2022-04-07
 */
@Api(value = "TestController", description = "系统调用", tags = {"用户管理"})
@RestController
@RequestMapping("/odsTest")
public class TestController extends BaseController
{

    /**
     * 查询劳派员工信息列表
     */
//    @RequiresPermissions("east:lpyg:list")
    @ApiOperation("测试被调用")
    @GetMapping("/list/{username}")
    public TableDataInfo list(@PathVariable("username") String username)
    {
        ArrayList<String> list = new ArrayList<>();
        list.add(username);
        list.add("夏日漱石");
        list.add("夕情难追");
        list.add("Summer");
        list.add("Rain after summer");
        return getDataTable(list);
    }

}
