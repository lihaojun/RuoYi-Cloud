package com.ruoyi.east.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 劳派员工信息对象 t_lpyg
 * 
 * @author lhj
 * @date 2022-04-07
 */
public class ILpyg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 员工姓名 */
    @Excel(name = "员工姓名")
    private String ygxm;

    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date csrq;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String lxdh;

    /** 操作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date sjc;

    /** $column.columnComment */
    private Long lpygId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String delFlag;

    public void setYgxm(String ygxm) 
    {
        this.ygxm = ygxm;
    }

    public String getYgxm() 
    {
        return ygxm;
    }
    public void setCsrq(Date csrq) 
    {
        this.csrq = csrq;
    }

    public Date getCsrq() 
    {
        return csrq;
    }
    public void setLxdh(String lxdh) 
    {
        this.lxdh = lxdh;
    }

    public String getLxdh() 
    {
        return lxdh;
    }
    public void setSjc(Date sjc) 
    {
        this.sjc = sjc;
    }

    public Date getSjc() 
    {
        return sjc;
    }
    public void setILpygId(Long lpygId)
    {
        this.lpygId = lpygId;
    }

    public Long getILpygId()
    {
        return lpygId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("ygxm", getYgxm())
            .append("csrq", getCsrq())
            .append("lxdh", getLxdh())
            .append("sjc", getSjc())
            .append("lpygId", getILpygId())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
