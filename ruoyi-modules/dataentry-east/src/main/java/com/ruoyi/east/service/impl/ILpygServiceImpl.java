package com.ruoyi.east.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.east.domain.ILpyg;
import com.ruoyi.east.mapper.ILpygMapper;
import com.ruoyi.east.service.ILpygService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 劳派员工信息Service业务层处理
 * 
 * @author lhj
 * @date 2022-04-07
 */
@Service
public class ILpygServiceImpl implements ILpygService
{
    @Autowired
    private ILpygMapper lpygMapper;

    /**
     * 查询劳派员工信息
     * 
     * @param lpygId 劳派员工信息主键
     * @return 劳派员工信息
     */
    @Override
    public ILpyg selectILpygByILpygId(Long lpygId)
    {
        return lpygMapper.selectILpygByILpygId(lpygId);
    }

    /**
     * 查询劳派员工信息列表
     * 
     * @param lpyg 劳派员工信息
     * @return 劳派员工信息
     */
    @Override
    public List<ILpyg> selectILpygList(ILpyg lpyg)
    {
        return lpygMapper.selectILpygList(lpyg);
    }

    /**
     * 新增劳派员工信息
     * 
     * @param lpyg 劳派员工信息
     * @return 结果
     */
    @Override
    public int insertILpyg(ILpyg lpyg)
    {
        lpyg.setCreateTime(DateUtils.getNowDate());
        return lpygMapper.insertILpyg(lpyg);
    }

    /**
     * 修改劳派员工信息
     * 
     * @param lpyg 劳派员工信息
     * @return 结果
     */
    @Override
    public int updateILpyg(ILpyg lpyg)
    {
        lpyg.setUpdateTime(DateUtils.getNowDate());
        return lpygMapper.updateILpyg(lpyg);
    }

    /**
     * 批量删除劳派员工信息
     * 
     * @param lpygIds 需要删除的劳派员工信息主键
     * @return 结果
     */
    @Override
    public int deleteILpygByILpygIds(Long[] lpygIds)
    {
        return lpygMapper.deleteILpygByILpygIds(lpygIds);
    }

    /**
     * 删除劳派员工信息信息
     * 
     * @param lpygId 劳派员工信息主键
     * @return 结果
     */
    @Override
    public int deleteILpygByILpygId(Long lpygId)
    {
        return lpygMapper.deleteILpygByILpygId(lpygId);
    }
}
