package com.ruoyi.east.controller;
/**
 * @author 李成功
 * @create 2022-04-2022/4/8
 */

import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.ods.api.RemoteTestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName RemoteTestController
 * @Description 测试服务调用
 * @Author 李成功
 * @Date 2022/4/8 14:26
 */
@Api(value = "RemoteTestController", description = "系统调用", tags = {"用户管理"})
@RestController
@RequestMapping("/remoteTest")
public class RemoteTestController
{
    @Autowired
    private RemoteTestService remoteTestService;


    /**
     * 获取当前用户信息
     */
//    @InnerAuth
    @ApiOperation("测试调用")
    @GetMapping("/test/{username}")
    public TableDataInfo info(@PathVariable("username") String username)
    {
        return remoteTestService.getTestInfo(username);
    }
}
