package com.ruoyi.east.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.east.domain.ILpyg;
import com.ruoyi.east.service.ILpygService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 劳派员工信息Controller
 * 
 * @author lhj
 * @date 2022-04-07
 */
@RestController
@RequestMapping("/lpyg")
public class ILpygController extends BaseController
{
    @Autowired
    private ILpygService lpygService;

    /**
     * 查询劳派员工信息列表
     */
    @RequiresPermissions("east:lpyg:list")
    @GetMapping("/list")
    public TableDataInfo list(ILpyg lpyg)
    {
        startPage();
        List<ILpyg> list = lpygService.selectILpygList(lpyg);
        return getDataTable(list);
    }

    /**
     * 导出劳派员工信息列表
     */
    @RequiresPermissions("east:lpyg:export")
    @Log(title = "劳派员工信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ILpyg lpyg)
    {
        List<ILpyg> list = lpygService.selectILpygList(lpyg);
        ExcelUtil<ILpyg> util = new ExcelUtil<ILpyg>(ILpyg.class);
        util.exportExcel(response, list, "劳派员工信息数据");
    }

    /**
     * 获取劳派员工信息详细信息
     */
    @RequiresPermissions("east:lpyg:query")
    @GetMapping(value = "/{lpygId}")
    public AjaxResult getInfo(@PathVariable("lpygId") Long lpygId)
    {
        return AjaxResult.success(lpygService.selectILpygByILpygId(lpygId));
    }

    /**
     * 新增劳派员工信息
     */
    @RequiresPermissions("east:lpyg:add")
    @Log(title = "劳派员工信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ILpyg lpyg)
    {
        return toAjax(lpygService.insertILpyg(lpyg));
    }

    /**
     * 修改劳派员工信息
     */
    @RequiresPermissions("east:lpyg:edit")
    @Log(title = "劳派员工信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ILpyg lpyg)
    {
        return toAjax(lpygService.updateILpyg(lpyg));
    }

    /**
     * 删除劳派员工信息
     */
    @RequiresPermissions("east:lpyg:remove")
    @Log(title = "劳派员工信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{lpygIds}")
    public AjaxResult remove(@PathVariable Long[] lpygIds)
    {
        return toAjax(lpygService.deleteILpygByILpygIds(lpygIds));
    }
}
