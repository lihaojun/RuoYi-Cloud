package com.ruoyi.east.mapper;

import com.ruoyi.east.domain.ILpyg;

import java.util.List;

/**
 * 劳派员工信息Mapper接口
 * 
 * @author lhj
 * @date 2022-04-07
 */
public interface ILpygMapper
{
    /**
     * 查询劳派员工信息
     * 
     * @param lpygId 劳派员工信息主键
     * @return 劳派员工信息
     */
    public ILpyg selectILpygByILpygId(Long lpygId);

    /**
     * 查询劳派员工信息列表
     * 
     * @param lpyg 劳派员工信息
     * @return 劳派员工信息集合
     */
    public List<ILpyg> selectILpygList(ILpyg lpyg);

    /**
     * 新增劳派员工信息
     * 
     * @param lpyg 劳派员工信息
     * @return 结果
     */
    public int insertILpyg(ILpyg lpyg);

    /**
     * 修改劳派员工信息
     * 
     * @param lpyg 劳派员工信息
     * @return 结果
     */
    public int updateILpyg(ILpyg lpyg);

    /**
     * 删除劳派员工信息
     * 
     * @param lpygId 劳派员工信息主键
     * @return 结果
     */
    public int deleteILpygByILpygId(Long lpygId);

    /**
     * 批量删除劳派员工信息
     * 
     * @param lpygIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteILpygByILpygIds(Long[] lpygIds);
}
