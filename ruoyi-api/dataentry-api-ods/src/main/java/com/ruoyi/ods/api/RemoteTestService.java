package com.ruoyi.ods.api;

import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.ods.api.factory.RemoteTestFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author 李成功
 * @create 2022-04-2022/4/8
 */
@FeignClient(contextId = "remoteTestService", value = ServiceNameConstants.ODS_SERVICE, fallbackFactory = RemoteTestFallbackFactory.class)
public interface RemoteTestService
{
    /**
     * 通过用户名查询用户信息
     *
     * @param username 用户名
     * @return 结果
     */
    @GetMapping(value = "/odsTest/list/{username}")
//    @RequestLine(value = "GET /odsTest/list/{username}")
    TableDataInfo getTestInfo(@PathVariable("username") String username);
}
