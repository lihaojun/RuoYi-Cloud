package com.ruoyi.ods.api.factory;
/**
 * @author 李成功
 * @create 2022-04-2022/4/8
 */


import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.ods.api.RemoteTestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @ClassName RemoteTestFallbackFactory
 * @Description ODS服务降级处理
 * @Author 李成功
 * @Date 2022/4/8 14:21
 */
@Component
public class RemoteTestFallbackFactory implements FallbackFactory<RemoteTestService>
{
    private static final Logger log = LoggerFactory.getLogger(RemoteTestFallbackFactory.class);

    @Override
    public RemoteTestService create(Throwable throwable)
    {
        log.error("ODS服务调用失败:{}", throwable.getMessage());
        return new RemoteTestService()
        {
            @Override
            public TableDataInfo getTestInfo(String username)
            {
                TableDataInfo dataInfo = new TableDataInfo();
                dataInfo.setCode(Constants.FAIL);
                dataInfo.setMsg("ODS服务调用失败:" + throwable.getMessage());
                return dataInfo;
            }
        };
    }
}