-- ----------------------------
-- Table structure for t_lpyg
-- ----------------------------
CREATE TABLE `t_lpyg`  (
   `YGXM` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工姓名',
   `CSRQ` datetime(0) NOT NULL COMMENT '出生日期',
   `LXDH` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
   `SJC` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
   `LPYG_ID` decimal(20, 0) NOT NULL COMMENT '劳派员工ID',
   `DEL_FLAG` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除标记',
   `CREATE_BY` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
   `CREATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
   `UPDATE_BY` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
   `UPDATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
   PRIMARY KEY (`LPYG_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '劳派员工信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_lpyg
-- ----------------------------
INSERT INTO `t_lpyg` VALUES ('示例数据', '1998-09-10 00:00:00', '15822001376', '2021-08-19 16:20:04', 1, '0', 'GHGZB', NULL, 'admin', NULL);
INSERT INTO `t_lpyg` VALUES ('Davao NO.1', '2003-12-03 08:00:00', '15678764577', '2021-08-19 16:19:05', 2, '0', 'GHGZB', NULL, 'admin', '2022-01-13 14:28:18');
INSERT INTO `t_lpyg` VALUES ('发短信给你是', '1998-09-10 00:00:00', '15822001376', '2021-07-06 14:35:37', 3, '0', 'GHGZB', NULL, 'admin', NULL);
INSERT INTO `t_lpyg` VALUES ('法', '2082-04-29 00:00:00', '15825653625', '2021-07-06 14:35:37', 4, '0', 'GHGZB', NULL, NULL, NULL);
INSERT INTO `t_lpyg` VALUES ('阿斯顿飞过', '2019-10-03 00:00:00', '15663564545', '2021-07-06 14:35:37', 5, '0', 'GHGZB', NULL, 'admin', NULL);
INSERT INTO `t_lpyg` VALUES ('大王来巡山', '2021-08-01 00:00:00', '17889892525', '2021-08-19 16:20:35', 6, '0', 'GHGZB', NULL, 'admin', NULL);
INSERT INTO `t_lpyg` VALUES ('示例数据2', '1998-09-10 00:00:00', '15822001376', '2021-08-19 16:25:34', 7, '0', 'GHGZB', NULL, 'admin', NULL);
INSERT INTO `t_lpyg` VALUES ('大铁锤', '1970-04-04 00:00:00', '17746532654', '2021-08-19 16:25:34', 8, '0', 'GHGZB', NULL, 'admin', NULL);
INSERT INTO `t_lpyg` VALUES ('示例数据3', '1998-09-10 00:00:00', '15822001366', '2021-08-19 16:25:47', 9, '0', 'GHGZB', NULL, 'admin', NULL);
INSERT INTO `t_lpyg` VALUES ('盗跖', '1999-03-31 00:00:00', '17746532654', '2021-08-19 16:25:47', 10, '0', 'GHGZB', NULL, 'admin', NULL);
INSERT INTO `t_lpyg` VALUES ('阿斯顿飞过', '2019-10-02 08:00:00', '15663564545', NULL, 1481201377664290818, '0', 'admin', '2022-01-12 17:48:22', NULL, NULL);
INSERT INTO `t_lpyg` VALUES ('少司命', '2022-01-12 08:00:00', '15848482536', NULL, 1481513691047182338, '0', 'admin', '2022-01-13 14:29:23', NULL, NULL);
INSERT INTO `t_lpyg` VALUES ('多发点', '1993-03-04 00:00:00', '13737933670', NULL, 1491722566526439426, '0', 'admin', NULL, 'admin', NULL);
INSERT INTO `t_lpyg` VALUES ('的风格去', '2002-03-02 00:00:00', '15509534650', NULL, 1491722566765514753, '0', 'admin', NULL, 'admin', NULL);
INSERT INTO `t_lpyg` VALUES ('二哥', '2001-02-05 00:00:00', '15822001365', NULL, 1491722877211119618, '0', 'admin', NULL, 'admin', NULL);
INSERT INTO `t_lpyg` VALUES ('Davao NO.1', '2003-12-03 00:00:00', '15678764577', NULL, 1493763948728426497, '0', 'admin', NULL, 'admin', NULL);
INSERT INTO `t_lpyg` VALUES ('阿斯顿飞过', '2019-10-02 00:00:00', '15663564545', NULL, 1493763949256908802, '0', 'admin', NULL, 'admin', NULL);
INSERT INTO `t_lpyg` VALUES ('少司命', '2022-01-12 00:00:00', '15848482536', NULL, 1493763949294657538, '0', 'admin', NULL, 'admin', NULL);

SET FOREIGN_KEY_CHECKS = 1;