import request from '@/utils/request'

// 查询劳派员工信息列表
export function listLpyg(query) {
  return request({
    url: '/east/lpyg/list',
    method: 'get',
    params: query
  })
}

// 查询劳派员工信息详细
export function getLpyg(lpygId) {
  return request({
    url: '/east/lpyg/' + lpygId,
    method: 'get'
  })
}

// 新增劳派员工信息
export function addLpyg(data) {
  return request({
    url: '/east/lpyg',
    method: 'post',
    data: data
  })
}

// 修改劳派员工信息
export function updateLpyg(data) {
  return request({
    url: '/east/lpyg',
    method: 'put',
    data: data
  })
}

// 删除劳派员工信息
export function delLpyg(lpygId) {
  return request({
    url: '/east/lpyg/' + lpygId,
    method: 'delete'
  })
}
